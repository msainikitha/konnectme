import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  homeData = [
    {
      heading: 'WIN A CHANCE TO MEET CRICKETER SUNIL ‘SUNNY’ GAVASKAR',
      para: 'And raise fund for heart surgeries through Heart Foundations',
      img: '../../../assets/img/image29.png',
    },
    {
      heading: 'WIN A TRIP TO GOD’S OWN COUNTRY KERALA AND ENJOY NATURE AT ITS BEST',
      para: 'And help raise money to educate the needy',
      img: '../../../assets/img/Kerala.png',
    },
    {
      heading: 'WIN A VOYAGE OF THE GLACIERS & ENJOY UNTAMED BEAUTY OF ALASKA',
      para: 'And help raise fund for “Home for All” Initiative',
      img: '../../../assets/img/Alaska.png',
    },
   
  ];


}
