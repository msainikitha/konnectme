import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

export interface PeriodicElement {
  eventName: string;
  price: string;
  registrants: number;
  totalPrice: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {eventName: 'AIM for Seva', price: '$300', registrants: 1, totalPrice: 300},
  {eventName: 'AIM for Seva', price: '$400', registrants: 3, totalPrice: 400},
  {eventName: 'AIM for Seva', price: '$500', registrants: 2, totalPrice: 500},
  {eventName: 'AIM for Seva', price: '$600', registrants: 1, totalPrice: 600},
];

@Component({
  selector: 'app-preview-registrant',
  templateUrl: './preview-registrant.component.html',
  styleUrls: ['./preview-registrant.component.css']
})
export class PreviewRegistrantComponent implements OnInit {

  registrants = [
    {
      id:'1',
      price: 300,
      type:'Non-Profit Registration',
      seva:'AIM for Seva',
      name:'Siva Kumar',
      email:"sivak@gmail.com,karthick@gmail.com",
      phone:"+134677654,+185446789",
      place:"SivaFoundation"
    },
    {
      id:'2',
      price: 400,
      type:'Non-Profit Registration',
      seva:'AIM for Seva',
      name:'Siva Kumar',
      email:"sivak@gmail.com,karthick@gmail.com",
      phone:"+134677654,+185446789",
      place:"SivaFoundation"
    },
    {
      id:'3',
      price: 500,
      type:'Non-Profit Registration',
      seva:'AIM for Seva',
      name:'Siva Kumar',
      email:"sivak@gmail.com,karthick@gmail.com",
      phone:"+134677654,+185446789",
      place:"SivaFoundation"
    },
    {
      id:'4',
      price: 600,
      type:'Non-Profit Registration',
      seva:'AIM for Seva',
      name:'Siva Kumar',
      email:"sivak@gmail.com,karthick@gmail.com",
      phone:"+134677654,+185446789",
      place:"SivaFoundation"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  public calculateTotal() {
    return this.registrants.reduce((accum, curr) => accum + curr.price, 0);
  }

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;

  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 600,
    navText: ['&#8249', '&#8250;'],
    responsive: {
      0: {
        items: 1 
      },
      400: {
        items: 2
      },
      760: {
        items: 3
      }
    },
    nav: true
  }

  


}
