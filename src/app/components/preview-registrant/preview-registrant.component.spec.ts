import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewRegistrantComponent } from './preview-registrant.component';

describe('PreviewRegistrantComponent', () => {
  let component: PreviewRegistrantComponent;
  let fixture: ComponentFixture<PreviewRegistrantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreviewRegistrantComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreviewRegistrantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
